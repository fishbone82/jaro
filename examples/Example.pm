package Example;
use strict;
use warnings;
use lib '../lib';
use Jaro;# config => '/etc/test.cfg';
use Example::Root;

# uwsgi --plugins psgi --http-socket-modifier1 5 --http-socket :5000 -M -p 1 --psgi Example.psgi -m

config(
    views => {
        TT => {
            INCLUDE_PATH    => "../htdocs/templates",
            # WRAPPER         => "layout.tt2",
        },
        'Text::Xslate' => {
            syntax      => 'TTerse',
            path        => "../htdocs/templates",
            #cache_dir   => "../htdocs/templates/cache",
        },
    },
    plugins => [
        {
            name => 'Static',
            opts => {
                root            => "../htdocs",
                pass_through    => 1,
            },
        },
        {
            name => 'Session',
            opts => {
                store => "File",
            },
        },
    ],
);

# Enable access logs via Plack::Midleware::AccessLog
#enable_plugin 'AccessLog', logger => Log::Dispatch->new(...);

# Sessions via Plack::Middleware::Session
#enable_plugin 'Session', store => 'File';

sub my_upper {return uc shift};

sub plain :Action( path => '/plain' ) {
    return "plain text";
}

sub error_404 :Action( is_error_page => 404, view => { class => 'TT', template => '404.tt2' }) {
    my $self = shift;
    return { action => $self };
}
sub error_500 :Action( is_error_page => 500, view => { class => 'TT', template => '500.tt2' }) {
    my $self = shift;
    return { action => $self };
}

sub xslate:Action(
    path => '/xslate',
    view => { class => 'Text::Xslate', template => 'root.tt2' },
) {
    my $self = shift;
    config->{views}->{'Text::Xslate'}->{function}->{my_upper} = \&my_upper;
    return { name => $self->request->parameters->{name} || 'Anonymous' };
}

sub foo :Action(
    method => 'GET',
    path => '/foo/*bar',
    view => { class => 'JSON' },
) {
    my $self = shift;
    my $bar = $self->request->parameters->{bar};
    #$self->redirect("http://ya.ru");
    config(foo => 'bar');
    $self->logger->debug("!! logger debug !!");
    warn "I am a warning";
    return { bar => $bar };
}

psgi();
