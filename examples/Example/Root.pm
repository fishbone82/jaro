package Example::Root;
use strict;
use warnings;

sub root :Action(
    method => [qw/HEAD GET/],
    path => ['/', '/root'],
    view => { class => 'TT' },
    before => \&before,
) {
    my $self = shift;
    $self->response->header('X-Foo' => 'bar');
    #die "I died! Look at the pretty 500!\n";
    $self->logger->debug("Wow! It works!");

    # override view example
    $self->route->{view}->{template} = 'root.tt2';

    return {
        page => { title => 'Greetings!' },
        name => $self->request->parameters->{name} || 'Anonymous',
    };
}

sub before {
    my $self = shift;
#    warn Dumper $self;
}

1;
