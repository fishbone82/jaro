package MyApp;
use strict;
use warnings;
use lib '../lib';
use Jaro;
use Data::Dumper;

# This is an action which runs at /hello and returns plain text
sub index :Action(
    path => '/hello/:name/*lastname'
) {
    my $self = shift;
    my $name = $self->params->get('name');
    my $lastname = $self->params->get('lastname');
    my $fullname = $name . ($lastname ? " $lastname" : "");
    warn $self->uri_for('/hello/Ivan', args => {foo => 1, bar => [2,3,4]});
    return "Hello there, $fullname!";
}

# sub index :Action(
#     path => '/hello'
# ) {
#     my $self = shift;
#     return "Hello world!";
# }

# # This is an action which runs at /blah and returns json
# sub blah_blah :Action(
#     view => {class => 'JSON'}, # Jaro::View::JSON has to be installed
#     path => '/blah'
# ) {
#     my $self = shift;
#     return {foo => 'bar'};
# }
return MyApp->psgi;
