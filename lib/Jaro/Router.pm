package Jaro::Router;
use strict;
use warnings;
use Jaro::Config;
use Jaro::Logger;
use Module::Load;
use parent qw /Class::Data::Inheritable/;
__PACKAGE__->mk_classdata(routes => []);
__PACKAGE__->mk_classdata(error_routes => {});
__PACKAGE__->mk_classdata(routes_cached => {});

sub logger { return Jaro::Logger::logger}

sub process_path {
    my ($class, $path) = @_;
    #1. Remove slashes
    $path =~ s!^/!!;
    $path =~ s!/$!!;
    my $re = '^/';
    foreach my $slug (split m!/!, $path) {
        if ($slug !~ m!^[:*]!) {
            $re .= $slug . '/';
        }
        elsif ($slug =~ m!^:(.+)!) {
            $re .= "(?'$1'[^/]+)/";
        }
        elsif ($slug =~ m!^\*(.+)!) {
            $re .= "?(?'$1'[^/]+)?/";
        }
    }
    $re =~ s!/$!/?!;
    $re .= '$';
    return $re;
}

sub route {
    my ($class, $action) = @_;
    my $found_route = undef;

    # Routing: Trying to find a route in cache
    my $cache_key = $action->request->method . $action->request->path_info;
    if (my $cached = $class->routes_cached->{$cache_key}) {
        $found_route = $cached->{route};
        $action->request->parameters->{$_} = $cached->{backref}->{$_} foreach keys %{$cached->{backref}};
    } else {
        # Routing: Looking up the route through all known routes
        foreach my $route (@{$class->routes}) {
            foreach my $path (@{$route->{path}}) {
                my @methods = ref $route->{method} ? @{$route->{method}} : ($route->{method} || 'GET');
                next unless grep { $action->request->method eq $_} @methods;
                if ($action->request->path_info =~ qr/$path/) {
                    my %backref = %+;
                    $action->request->parameters->{$_} = $backref{$_} foreach keys %backref;
                    $found_route = $route;

                    # Put route to cache
                    $class->routes_cached->{$cache_key} = {
                        ts      => time,
                        route   => $route,
                        backref => \%backref,
                    } unless $route->{no_route_cache};
                    last;
                }
            }
            last if $found_route;
        }
    }

    if ($found_route) {
        $action->route($found_route);
        if(config->{log_route}) {
            my $symbol = *{$found_route->{symbol}};
            $symbol =~ s!^\*!!;
            $symbol =~ m!::([^:]+)$!;
            $symbol = $1;
            logger->debug(sprintf "[ROUTER] Routed to sub '%s' of package '%s' (%s:%s)",
                $symbol,
                $found_route->{class},
                $found_route->{filename},
                $found_route->{linenum},
            );
        }
    } else {
        if(config->{log_route}) {
            logger->debug("[ROUTER] No route found in application actions.");
        }
    }

    $action->add_profiling('Routing');

    return;
}

sub process_custom_error {
    my ($class, $action) = @_;
    my $error_route = $class->error_routes->{$action->response->status};
    if($error_route) { # there is custom error!
        if(config->{log_route}) {
            my $symbol = *{$error_route->{symbol}};
            $symbol =~ s!^\*!!;
            $symbol =~ m!::([^:]+)$!;
            $symbol = $1;
            logger->debug(sprintf "[ROUTER] Custom %s error page was routed to sub '%s' in package '%s' (%s:%s)",
                $action->response->status,
                $symbol,
                $error_route->{class},
                $error_route->{filename},
                $error_route->{linenum},
            );
        }
        $action->route($error_route);
        eval {
            $action->dont_render(0);
            $action->data($action->route->{coderef}->($action) || undef);
            $action->render;
        };
        warn $@ if $@;
    }
    return;
}

sub load_action_callback {
    my ($class, $data) = @_;
    if($data->{is_error_page}) {
        $class->error_routes->{$data->{is_error_page}} = $data;
    } else {
        push @{$class->routes}, $data;
    }
    return;
}

sub init_callback {
    my $class = shift;
    my $loaded_views = {};
    foreach my $route (@{$class->routes}) {
        my $class_shortname = ($route->{view} ? $route->{view}->{class} : '') || config->{default_view};
        if ($class_shortname) {
            my $view_class = get_view_class($class_shortname);
            unless($loaded_views->{$view_class}) {
                load $view_class;
                $loaded_views->{$view_class} = 1;
            }
        }
    }
    return;
}

sub get_view_class {
    my $class = shift;
    return '' unless $class;
    return $class =~ m!^\+! ? $class : "Jaro::View::" . $class;
}

1;
