package Jaro::Config;
use strict;
use warnings;
use FindBin qw/$Bin/;
use Hash::Merge;
use Data::Dumper;
use parent qw /Exporter/;
our @EXPORT = qw/config/;

my $config;

my $merger = Hash::Merge->new('RIGHT_PRECEDENT');

sub config {
    my %update = @_;
    if (scalar(keys %update)) {
        # update config
        my $new_config = $merger->merge($config, \%update);
        $config = $new_config;
    }
    return $config;
};

sub load_config {
    my %opts = @_;
    my $config_filename = $opts{class} ? "$Bin/$opts{class}.conf" : $opts{file};
    my %config = ();
    unless (-r $config_filename) {
        if ($opts{file}) {
            die "Can't load config from $config_filename";
        } else {
            warn "Could't load config from $config_filename but it might be OK";
        }
    } else {
        warn "Loading config from $config_filename\n";
        %config = do($config_filename);
        die "Config $config_filename parse error: $@\n" if $@;
    }
    $config = $merger->merge(default_config(), \%config);
    return $config;
}

sub default_config  {
    return {
        default_plugins  =>  [
            {   name => 'ContentLength' },
            {
                name => 'Static',
                opts => {
                    path => qr!^/static/!,
                },
            },
        ],
        logger  =>  {
            #outputs => { Screen => { min_level => "warning" } },
            outputs => { Screen => { min_level => "debug" } },
            format  => '%T.%t %P [%L] %M %m %u',
            catch_warn => 1,
        },
        router  => {
            class => 'Jaro::Router',
        },
        default_view => 'PlainText',
        profiling => { enabled => 1 },
        uri_for => {
            schemeless  => 1,
            absolute    => 1,
        },
        log_route => 1,
        default_http_code_ok        => 200,
        default_http_code_not_found => 404,
        default_http_code_error     => 500,
        default_http_code_redirect  => 302,
    };
}

1;
