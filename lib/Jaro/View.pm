package Jaro::View;
use strict;
use warnings;
use Jaro::Config;
use parent qw/Class::Data::Inheritable/;

__PACKAGE__->mk_classdata(content_type => undef);

sub short_name {
    my $class = shift;
    $class =~ m!Jaro::View::(.*)$!;
    return $1 || $class;
}

sub get_config_for_class {
    my $class = shift;
    return config->{views}->{$class->short_name};
}

sub content_type {};

1;