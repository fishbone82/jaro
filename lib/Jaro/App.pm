use strict;
use warnings;
use Try::Tiny;
use Jaro::Action;
use parent qw/Exporter/;

our @EXPORT_OK = qw/app/;

sub app {
    my ($env) = @_;

    # Create an action object
    my $action = Jaro::Action->new($env);

    try {
        $action->do_routing();
        if($action->route) {
            # do we really need this 'before' stuff at all?
            if ($action->route->{before}) {
                $action->route->{before}->($action);
            }
            my $action_data = $action->route->{coderef}->($action);
            $action->data($action_data);
        } else {
            # 404
            $action->response->status(404);
            $action->dont_render(1);
            $action->response->body("Not Found");
        }
    }
    catch {
        my $err = shift;
        # PLEASE PUT SOME USEFUL CODE HERE
        unless (blessed $err) {
            # 500
            # This behavior should be set via the action configuration
            $action->dont_render(1);
            $action->response->status(500);
            $action->response->body("Internal Server Error"); # $err?
            $action->logger->error($err);
        }
    } finally {
        $action->add_profiling('Action processing');
    };

    $action->render();

    # Log the profiling data
    $action->log_profiling();

    # All done.
    return $action->response->finalize;
};

1;
