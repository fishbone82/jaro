package Jaro::Exceptions;
use strict;
use warnings;
use Exception::Class(
    'Jaro::Exception',
    'Jaro::Exception::Redirect' => {
        isa => 'Jaro::Exception',
    },
);

1;
