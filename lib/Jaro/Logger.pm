package Jaro::Logger;
use strict;
use warnings;
use Log::Dispatch;
use Jaro::Config;
use POSIX();
use Time::HiRes;

my $logger;

sub logger { return $logger };

# debug info notice warning error critical alert emergency

sub init {
    # init error log
    my %error_log_options = (outputs => []);

    while (my ($output, $options) = each %{config->{logger}->{outputs}}) {
        push @{$error_log_options{outputs}}, [$output, %{$options}];
    }
    return unless scalar @{$error_log_options{outputs}};

    $logger = Log::Dispatch->new(%error_log_options);
    $logger->add_callback(\&log_callback);

    # Catch warnings
    if (config->{logger}->{catch_warn} && $logger->is_warning) {
        $SIG{__WARN__} = sub { $logger->warning(shift) };
    }

    $logger->debug("Logger initialized");
    return;
}

# %T.%t %P [%L] %M %m %u
sub log_callback {
    my %p = @_;
    my $orig_message = $p{message};
    $orig_message =~ s/\n//g; # ORLY?

    my $format = config->{logger}->{format} . "\n";
    my @args = ();
    while(config->{logger}->{format} =~ m!(%[TtPLMmu])!g) {
        my $arg = $1;
        my $replace;
        if($arg eq '%T') {
            $replace = POSIX::strftime("%d-%m-%Y %H:%M:%S", localtime);
        } elsif($arg eq '%t') {
            ($replace) = sprintf("%.3f", Time::HiRes::time()) =~ m!\.(.+)$!;
        } elsif($arg eq '%P') {
            $replace = $$;
        } elsif($arg eq '%L') {
            $replace = $p{level};
        } elsif($arg eq '%M') {
            $replace = $orig_message;
        } elsif($arg eq '%m') {
            $replace = $logger->{current_action} ? $logger->{current_action}->request->method : '';
        } elsif($arg eq '%u') {
            $replace = $logger->{current_action} ? $logger->{current_action}->request->uri : '';
        }
        push @args, $replace;
        $format =~ s/$arg/%s/g;
    }

    return sprintf($format, @args);
};

1;
