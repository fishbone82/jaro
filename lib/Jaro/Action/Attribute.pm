package Jaro::Action::Attribute;
use strict;
use warnings;
use Jaro::Config;

sub UNIVERSAL::Action :ATTR(BEGIN) {
    my ($package, $symbol, $referent, $attr, $attr_data, $phase, $filename, $linenum) = @_;
    unless( ref $attr_data eq 'ARRAY') {
        $attr_data = [];
    }
    my %data = (
        class       => $package,
        coderef     => $referent,
        symbol      => $symbol,
        filename    => $filename,
        linenum     => $linenum,
    );
    my %cmd_data = @$attr_data;
    my $router = config->{router}->{class};
    if($cmd_data{path} || $cmd_data{is_error_page}) {
        if($cmd_data{path} && !$cmd_data{is_error_page}) {
            my @paths = ref $cmd_data{path} ? @{$cmd_data{path}} : $cmd_data{path};
            my @processed_paths = ();
            foreach my $path(@paths) {
                push @processed_paths, $router->process_path($path);
            }
            $cmd_data{path_raw} = $cmd_data{path};
            $cmd_data{path} = \@processed_paths;
        }
    } else {
        die "Neither 'path' nor 'is_error_page' attribute is set for action at $filename:$linenum";
    }
    %data = (%data, %cmd_data);

    # Call a router's callback with action data
    $router->load_action_callback(\%data);
    return;
}

1;
