package Jaro::Action;
use strict;
use warnings;
use Class::Accessor qw/moose-like/;
use Try::Tiny;
use Scalar::Util qw/blessed/;
use Time::HiRes qw/time/;
use Plack::Request;
use Jaro::Config;
use Jaro::Logger;
use Jaro::Exceptions;

has request     => (is => 'rw');
has response    => (is => 'rw');
has route       => (is => 'rw');
has data        => (is => 'rw');
has dont_render => (is => 'rw');

sub new {
    my ($class, $env) = @_;

    my $request = Plack::Request->new($env);

    my $self = bless {
        request  => $request,
        response => $request->new_response(config->{default_http_code_ok}),
    }, $class;

    # Start profiling
    $self->add_profiling('Action object is created');

    # FIXME maybe we need to instantiate an action logger object LINKED to the action instead of having the global one?
    # Set current action for the logger
    $self->logger->{current_action} = $self if $self->logger;
    return $self;
}

sub logger { return Jaro::Logger::logger }

sub redirect {
    my ($self, $url, $code) = @_;
    $code ||= config->{default_http_code_redirect};
    warn "Redirecting to $url with code $code\n";
    $self->response->redirect($url, $code);
    $self->dont_render(1);
    return Jaro::Exception::Redirect->throw;
}

sub view {
    my $self = shift;
    return $self->route->{view};
}

sub get_view_class {
    my $self = shift;
    my $class_short = ( $self->view ? $self->view->{class} : '') || config->{default_view};
    return '' unless $class_short;
    return $class_short =~ m!^\+! ? $class_short : "Jaro::View::" . $class_short;
}

sub render {
    my $self = shift;
    if ($self->dont_render) {
        my $router = config->{router}->{class};
        $router->process_custom_error($self); # FIXME: move it from router here?
        return;
    }

    if (my $view_class = $self->get_view_class()) {
        my $body;
        try {
            $body = $view_class->render($self);
            $self->response->content_type($view_class->content_type) if $view_class->content_type;
            $self->response->body($body);
        }
        catch {
            my $err = shift;
            # PLEASE PUT SOME USEFUL CODE HERE
            unless (blessed $err) {
                # 500
                # This behavior should be set via the action configuration
                $self->dont_render(1);
                $self->response->status(config->{default_http_code_error});
                $self->response->body($err);
                $self->logger->error("$view_class error: $err");
            }
        };
    } else {
        $self->response->status(config->{default_http_code_error});
        $self->response->body("500 :(");
        $self->logger->error("Action has no view class");
    }

    $self->add_profiling('Rendering');

    return;
}

sub params {
    my $self = shift;
    return $self->request->parameters;
}

sub profiling_info {
    my ($self, $dont_serialize) = @_;
    return unless config->{profiling}->{enabled};

    my $info = [];
    my $string = "";
    my @timings = @{$self->{timings}};
    my $prev;
    while ( @timings ) {
        my $time = shift @timings;
        my $event = shift @timings;
        unless ($prev) {
            $prev = $time;
            next;
        }
        my $delta = sprintf("%.3f", 1000*($time - $prev));
        push @$info, [$event, $delta];
        $string .= "$event: $delta ms; ";
        $prev = $time;
    }

    my $total = 1000*($self->{timings}->[-2] - $self->{timings}->[0]);
    push @$info, ['Total', sprintf("%.3f", $total)];
    $string .= "Total: $total ms.";
    return $dont_serialize ? $info : $string;
}

sub uri_for {
    my ($self, $uri, $p) = @_;
    unless (ref $p) {
        my %p = splice @_, 2;
        $p = \%p;
    }
    my $c = config->{uri_for};
    $c->{$_} = $p->{$_} foreach keys %$p;
    my $env = $self->request->env;
    my $uri_for = '';
    if ($c->{absolute}) {
        $uri_for .= $c->{schemeless} ? '//' : ($env->{'psgi.url_scheme'} . '://');
        my $host = $env->{HTTP_HOST};
        unless($host) {
            $host = $env->{SERVER_NAME};
            if(($env->{SERVER_PORT} != 80 && $env->{'psgi.url_scheme'} eq 'http') || ($env->{SERVER_PORT} != 443 && $env->{'psgi.url_scheme'} eq 'https')) {
                $host .= ":" . $env->{SERVER_PORT};
            }
        }
        $uri_for .= $host;
    }
    $uri_for .= $env->{SCRIPT_NAME} if $env->{SCRIPT_NAME};
    $uri_for .= $uri;
    if($p->{args}) {
        $uri_for .= '?';
        my $amp = '';
        for my $arg (keys %{$p->{args}}) {
            my $val = $p->{args}->{$arg};
            my $arg_type = ref $val;
            if(! $arg_type ) {
                $uri_for .= "$amp$arg=$val";
            } elsif ($arg_type eq 'ARRAY') {
                $uri_for .= "$amp$arg=$_" foreach @{$val};
            } else {
                die "Can't use HASHREF as argument value\n";
            }
            $amp='&';
        }
    }
    return $uri_for;
}

sub add_profiling {
    my ($self, $event) = @_;
    return unless config->{profiling}->{enabled};

    push @{$self->{timings}}, time, $event;
    return;
}

sub log_profiling {
    my ($self) = @_;
    return unless config->{profiling}->{enabled};

    $self->logger->debug("TIMINGS: " . $self->profiling_info());
    return;
}

sub do_routing {
    my ($self) = @_;
    my $router = config->{router}->{class};
    $router->route($self);
    return;
}

1;
