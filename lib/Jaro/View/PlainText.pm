package Jaro::View::PlainText;
use strict;
use warnings;
use parent qw/Jaro::View/;

__PACKAGE__->content_type('text/plain');

sub render {
    my ($class, $action, $view) = @_;
    my $raw_data = $action->data;
    die "Action return value must be a plaint text" if ref $raw_data; 
    return $raw_data;
}

1;