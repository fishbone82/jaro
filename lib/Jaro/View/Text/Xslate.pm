package Jaro::View::Text::Xslate;
use strict;
use warnings;
use Text::Xslate;
use parent qw/Jaro::View/;

our $VERSION = '0.01a';

sub content_type { return 'text/html' }

sub render {
    my ($class, $action) = @_;
    my $raw_data = $action->data;
    my $view = $action->view;
    my $template = $view->{template};
    my $rendered_data;
    my %class_config = %{ $class->get_config_for_class() || {} };
    my %action_config = %{ $view || {} };
    my %config = (%class_config, %action_config);
    delete $config{class};
    delete $config{template};
    $config{function}->{uri_for} = sub { return $action->uri_for(@_) };
    my $tx = Text::Xslate->new(%config);

    # Send some extra data to template
    $raw_data->{jaro} = { version => $Jaro::VERSION };

    $rendered_data = $tx->render($template, $raw_data);
    return $rendered_data;
}

1;
