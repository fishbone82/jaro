package Jaro::View::JSON;
use strict;
use warnings;
use JSON::XS;
use parent qw/Jaro::View/;

our $VERSION = '0.01a';

__PACKAGE__->content_type('application/json');

my $json = JSON::XS->new();

sub render {
    my ($class, $action) = @_;
    my $raw_data = $action->data;
    my $view = $action->view;
    my $json_data;
    if ($view && !$view->{utf8}) {
        $json_data = $json->utf8(0)->encode($raw_data);
    } else {
        $json_data = encode_json($raw_data);
    }
    return $json_data;
}

1;