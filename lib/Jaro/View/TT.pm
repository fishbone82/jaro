package Jaro::View::TT;
use strict;
use warnings;
use Template;
use parent qw/Jaro::View/;

our $VERSION = '0.01a';

__PACKAGE__->content_type('text/html');

sub render {
    my ($class, $action) = @_;
    my $raw_data = $action->data;
    my $view = $action->view;
    my $template = $view->{template};
    my $rendered_data;
    my %class_config = %{ $class->get_config_for_class() || {} };
    my %action_config = %{ $view || {} };
    my %config = (%class_config, %action_config);
    delete $config{class};
    delete $config{template};
    my $tt = Template->new({ OUTPUT => \$rendered_data, %config });

    # Send some extra data to template 
    $raw_data->{jaro} = { version => $Jaro::VERSION };
    $raw_data->{uri_for} = sub { $action->uri_for(@_) };

    my $result = $tt->process($template, $raw_data);
    die "Can't render template $template: $@" unless $result;
    return $rendered_data;
}

1;