package Jaro;
use strict;
use warnings;
use base qw/Exporter/;
use Plack::Builder;
use Attribute::Handlers;
use Module::Load;
use Data::Dumper;
use Jaro::App qw/app/;
use Jaro::Action::Attribute;
use Jaro::Config;
use Jaro::Logger;
use Jaro::Exceptions;
use Try::Tiny;
use Scalar::Util qw /blessed/;
use Data::Dumper;

our $VERSION = "0.01a";
our @EXPORT = qw/enable_plugin psgi config/;

sub logger { return Jaro::Logger::logger }

# This code runs once when app is starting
sub psgi {
    my ($class) = @_;

    # Take class from caller if empty
    $class = caller(0) unless $class;

    # Call a router's init callback
    # FIXME WTF??
    config->{router}->{class}->init_callback();

    # Init a logger
    Jaro::Logger->init();

    # Create a builder
    my $builder = Plack::Builder->new();

    # Enable plugins
    # FIXME is the order correct?
    enable_plugin(
        @{config->{plugins}},
        @{config->{default_plugins}},
    );

    foreach my $plugin ( @{$class::plugins} ) {
        $builder->add_middleware($plugin->{name}, %{$plugin->{opts}});
        logger->debug("Middleware '$plugin->{name}' has been enabled");
    }
    return $builder->wrap(\&app);
}

sub enable_plugin {
    my @plugins = @_;
    my $class = caller(0);
    push @{$class::plugins}, @plugins;
    return;
}

sub import {
    my ($class, %opts) = @_;

    # We use it to build a default config name. Maybe we don't need to do this whatsoever.
    my $user_app_class = caller(0);

    # Load config
    if ($class eq 'Jaro' && $opts{config}) {
        Jaro::Config::load_config(file => $opts{config}) unless config;
        @_ = ('Jaro'); #FIXME what is that for?
    } else {
        Jaro::Config::load_config(class => $user_app_class) unless config;
    }

    # Load Router class
    load config->{router}->{class};

    Jaro->export_to_level(1, @_);
    return;
}

1;
