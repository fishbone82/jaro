# **Jaro**
Jaro is a Plack-based web framework for Perl.

* It is simple, fast and lightweight.
* It is based on PSGI so there are variety of web-servers it could be run at, so you can use uWSGI/Starman/Corona/whatever to run your Jaro aplication.
* Jaro is easy-to-configure. It uses Perl-based config file syntax.
* It supports usage of Plack middlewares as plugins.
* Jaro is extendable - write your own middlewares, view classes or even routers if you need more freedom. Just put in config which module you want Jaro to use.

##  Quick start
Just look how simple it is to start your brand-new application MyApp.pm:
```
package MyApp;
use Jaro;

# This is an action which runs at /hello and returns plain text
sub index :Action(path => '/hello') {
    my $self = shift;
    my $name = $self->params->get('name') || 'anonymous';
    return "Hello there, $name!"
}

# This is an action which runs at /blah and returns json
sub blah_blah :Action(path => '/blah', view => {class => 'JSON'}) {
    my $self = shift;
    return {foo => 'bar'};
}

return MyApp->psgi;
```
Now save the file and use your favorite web-server to run it:
```
starman ./MyApp.pm
```
Then open browser and go to http://localhost:5000/?name=John

Most likely you'll see: 

    Hello there, John

For http://localhost:5000/blah you'll see 
```json
{"foo": "bar"}
```
Quite easy, isn't it? For more detailed information please visit [wiki pages](wiki/Home)