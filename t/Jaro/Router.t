#!/usr/bin/perl
use strict;
use warnings;
use Test::More;
use Test::Deep;
use Data::Dumper;
use_ok ('Jaro::Router');

my %data = (
    '/foo' => {
        ok => {
            '/foo'  => {},
            '/foo/' => {}
        },
        not =>['/foo/bar'],
    },
    '/foo/:bar' => {
        ok => {
            '/foo/133' => {bar => '133'},
            '/foo/bar' => {bar => 'bar'},
            '/foo/baz/'=> {bar => 'baz'},
        },
        not =>['/foo', '/foo/'],
    },
    '/foo/*bar' => {
        ok => {
            '/foo/133' => {bar => '133'},
            '/foo/bar' => {bar => 'bar'},
            '/foo/baz/'=> {bar => 'baz'},
            '/foo'     => {},
            '/foo/'    => {}
        },
        not => ['/baz', '/foo/bar/baz', '/foo/bar/134'],
    },
    '/foo/:bar/*baz' => {
        ok => {
            '/foo/133' => {bar => '133'},
            '/foo/bar' => {bar => 'bar'},
            '/foo/baz/'=> {bar => 'baz'},
            '/foo/234/23r' => {bar => '234', baz => '23r'},
            '/foo/34t/32r/'=> {bar => '34t', baz => '32r'},
        },
        not=> ['/foo/bar/baz/erg/', '/foo/bar/134/g43g'],
    },
    '/foo/*bar/:baz' => {
        ok => {
            '/foo/123' => {baz => 123},
            '/foo/123/qwe' => {bar =>123, baz => 'qwe'},
            '/foo/rewgt/' => {baz => 'rewgt'},
        },
        not=> ['/'],
    },
);

foreach my $route (keys %data) {
    my $re = Jaro::Router->process_path($route);
    foreach my $url (keys %{ $data{$route}{ok} }) {
        ok($url =~ $re, "$route didn't catch $url");
        cmp_deeply(\%+, $data{$route}{ok}{$url});
    }
    foreach my $url (@{$data{$route}{not}}) {
        ok($url !~ $re, "$route caught $url");
    }
}
done_testing;
